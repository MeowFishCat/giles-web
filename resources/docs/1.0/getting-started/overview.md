# Overview

---

- [Introduction](#introduction)
- [Laravel Installation](#laravel-installation)

<a name="introduction"></a>
## Introduction

With the Giles Laravel package, you can fire off Artisan commands directly from [Slack](https://slack.com) with no need to SSH.
Take advantage of Artisan and writing custom Artisan commands to deploy your app, send out emails and get quick site statistics.


<a name="laravel-installation"></a>
## Giles for Laravel Installation
### Prerequisites
You must have created a [Giles](https://giles.app) account, and the Giles app must be installed on your Slack team (done when creating an account).

### Getting Started
1. Simply require the package to get started.
    ```bash
    composer require jarker/giles
    ```
2. Login to [Giles](https://giles.app) and click 'Add Site', or select your site if you've already created one.
3. Click 'View App Key' and copy  it.
4. In your Laravel's `.env` file, add the following:
    ```bash
    GILES_APP_KEY=<YOUR APP KEY HERE>
    ```
    
**It is important that you do not share your Giles App Key with anyone.**

You will now be able to execute commands on your Laravel app via Slack using Giles.
