
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/meow.css') }}" rel="stylesheet">

    <!-- Styles -->
    <style>
        @import url('https://fonts.googleapis.com/css?family=Lato:400,700');
        .logo {
            font-weight: 700;
            font-family: 'Lato', sans-serif;
            color: #3366FF;
            padding: 0 15px 0 15px;
        }
        .slack-sign-in {
            border: 2px solid transparent;
            border-radius: 8px;
            transition: 0.3s;
        }
        .slack-sign-in:hover {
            border: 2px solid #795eff;
        }
        .row-100 {
            display: flex;
            flex-wrap: wrap;
        }
        .mwidth-100 {
            max-width: 100%;
        }
        .mauto-0 {
            margin: auto 0;
        }
    </style>
</head>
<body class="body-purple">
<div class="container">
    <div class="space-above-below">
        <div class="navigation navigation-with-buttons" id="navigation">
            <div class="navigation-right">
                <a href="#features">Features</a>
                <a href="#pricing">Pricing</a>
                <a href="/docs">Docs</a>
                <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot" class="button button-emphasis"><i class="fa fa-user"></i> Login</a>
                <a href="#" class="toggle-icon"><i class="fa fa-bars"></i></a>
            </div>
            <div class="logo"><i class="fa fa-terminal"></i> {{ config('app.name', 'Laravel') }}</div>
        </div>
    </div>

    <div class="space-above-below">
        <header class="header-left header-no-set-widths">
            <div class="row row-100">
                <div class="col-md-6 mauto-0">
                    <h1>Giles, Artisan Deploy.</h1>
                    <p>Enhance your DevOps workflow with Giles, allowing you to quickly manage your server with Laravel's Artisan straight from Slack.</p>
                    <br /><br />
                    <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot">
                        <img alt="Sign in with Slack" height="40" width="172" class="slack-sign-in"
                             src="https://platform.slack-edge.com/img/sign_in_with_slack.png"
                             srcset="https://platform.slack-edge.com/img/sign_in_with_slack.png 1x,
                        https://platform.slack-edge.com/img/sign_in_with_slack@2x.png 2x"
                        />
                    </a>
                </div>
                <div class="col-md-6">
                    <img class="image-with-shadow" src="{{ asset('image/giles-gif.gif') }}" alt="Giles in action" />
                </div>
            </div>
        </header>
    </div>
</div>
<section class="white" id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="feature">
                    <div class="icon big-icon"><i class="fa fa-terminal"></i></div>
                    <h2>Artisan</h2>
                    <p>Migrate, deploy, send emails, or bring down for maintenance, all without leaving Slack.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature">
                    <div class="icon big-icon"><i class="fa fa-shield"></i></div>
                    <h2>Permission Based Commands</h2>
                    <p>Larger team? Whitelist commands executable, and then choose which users can run them.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature">
                    <div class="icon big-icon"><i class="fa fa-plug"></i></div>
                    <h2>Extensive Capabilities</h2>
                    <p>Set up commands to run bash scripts or even return this week's statistics.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
                <div class="pricing-card">
                    <div class="heading">Basic</div>
                    <div class="price">Free</div>
                    <div class="period">Forever</div>
                    <ul>
                        <li>Full Artisan access</li>
                        <li>1 site</li>
                        <li>Unlimited commands</li>
                        <li>Unlimited users</li>
                    </ul>
                    <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot" class="button button-block"><i class="fa fa-arrow-right"></i> Get Started</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pricing-card">
                    <div class="heading">Professional</div>
                    <div class="price"><span class="currency">&dollar;</span>7.99</div>
                    <div class="period">Per month, or &dollar;79.99 billed annually</div>
                    <ul>
                        <li>Full Artisan access</li>
                        <li>Unlimited sites</li>
                        <li>Unlimited commands</li>
                        <li>Unlimited users</li>
                    </ul>
                    <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot" class="button button-success button-block"><i class="fa fa-arrow-right"></i> Get Started</a>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</section>
<footer class="white">
    <div class="container">
        <div class="pull-right">
            <a href="#features">Features</a>
            <a href="#pricing">Pricing</a>
            <a href="/docs">Docs</a>
            <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot">Login</a>
        </div>
        <strong>Giles.</strong> &copy; {{ date('Y') }} <a href="https://joshbarker.me" target="_blank">Josh Barker</a>
    </div>
</footer>
</body>
</html>
