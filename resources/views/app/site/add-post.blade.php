@extends('layouts.app')

@section('content')
    <style>
        .feature .icon {
            float: none!important;
        }
    </style>
    <h1>Add Site</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-plus"></i></div>
        <div class="heading">Site Information</div>
        <div class="content">
            <div class="feature feature-centered">
                <div class="icon big-icon"><i class="fa fa-check"></i></div>
                <h2>Site Saved</h2>
                <p>You can now manage your site's <a href="{{ route('app.site', [$site->id]) }}">commands</a> and who can access them.</p>
            </div>
        </div>
    </div>
@endsection
