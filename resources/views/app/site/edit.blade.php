@extends('layouts.app')

@section('content')
    <style>
        .form-group .tip {
            float: right;
            color: #666;
        }
        .form-group .error {
            margin: 5px 0;
            font-size: smaller;
            color: darkred;
        }
    </style>
    <h1>Edit Site: {{ $site->name }}</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-plus"></i></div>
        <div class="heading">Site Information</div>
        <div class="content">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <small class="tip">Must only be letters, no special characters or numbers. Used for determining which command to run a site on.</small>
                    <label for="name">Site ID</label>
                    <input type="text" class="form-element" placeholder="mywebsite" value="{{ old('name') ?: $site->name }}" name="name" id="name" required />
                    @error('name')
                        <span class="error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <small class="tip">Must include https://, must not include trailling slash. Used for executing the API call to run your commands.</small>
                    <label for="url">Site URL</label>
                    <input type="text" class="form-element" placeholder="https://mywebsite.com" value="{{ old('url') ?: $site->url }}" name="url" id="url" required />
                    @error('url')
                        <span class="error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="submit" class="button"><i class="fa fa-save"></i> Save Site</button>
                </div>
            </form>
        </div>
    </div>
@endsection
