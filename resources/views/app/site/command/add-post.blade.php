@extends('layouts.app')

@section('content')
    <style>
        .form-group .tip {
            float: right;
            color: #666;
        }
        .form-group .error {
            margin: 5px 0;
            font-size: smaller;
            color: darkred;
        }
        .user-row {
            border-bottom: 1px solid #eee;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        input[type=checkbox] {
            transform: scale(1.4);
            margin-top: 6px;
            margin-right: 6px;
        }
        .feature .icon {
            float: none!important;
        }
    </style>
    <h1>Add Command to {{ $site->name }}</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-plus"></i></div>
        <div class="heading">Add Command</div>
        <div class="content">
            <div class="feature feature-centered">
                <div class="icon big-icon"><i class="fa fa-check"></i></div>
                <h2>Command Saved</h2>
                <p><a href="{{ route('app.site.command.add', [$site->id]) }}">Add another</a> or <a href="{{ route('app.site', [$site->id]) }}">view all</a>.</p>
            </div>
        </div>
    </div>
@endsection
