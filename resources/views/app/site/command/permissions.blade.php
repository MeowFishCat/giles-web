@extends('layouts.app')

@section('content')
    <style type="text/css">
        .user-row {
            border-bottom: 1px solid #eee;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        input[type=checkbox] {
            transform: scale(1.4);
            margin-top: 6px;
            margin-right: 6px;
        }
    </style>
    <h1>Manage Permissions</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-shield"></i></div>
        <div class="heading">Users Allowed to Execute Command: <code>{{ $command->command }}</code></div>
        <div class="content">
            <form method="post">
                {{ csrf_field() }}

                @foreach ($users as $user)
                    @if (!$user['is_bot'])
                        <div class="user-row">
                            <div class="pull-right">
                                <input type="checkbox" name="permission[]" value="{{ $user['id'] }}" {{ isset($user['checked']) && $user['checked'] ? 'checked="checked"' : '' }} />
                            </div>
                            <code>{{ $user['real_name'] }}</code>
                        </div>
                    @endif
                @endforeach

                <button type="submit" class="button"><i class="fa fa-save"> </i> Save Permissions</button>
            </form>
        </div>
    </div>
@endsection
