@extends('layouts.app')

@section('content')
    <style>
        .form-group .tip {
            float: right;
            color: #666;
        }
        .form-group .error {
            margin: 5px 0;
            font-size: smaller;
            color: darkred;
        }
        .user-row {
            border-bottom: 1px solid #eee;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        input[type=checkbox] {
            transform: scale(1.4);
            margin-top: 6px;
            margin-right: 6px;
        }
    </style>
    <h1>Add Command to {{ $site->name }}</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-plus"></i></div>
        <div class="heading">Add Command</div>
        <div class="content">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <small class="tip">Just the name is necessary without any optional parameters. Example: <code>users:list</code>.</small>
                    <label for="name">Command name</label>
                    <input type="text" class="form-element" placeholder="users:list" value="{{ old('name') }}" name="name" id="name" required />
                    @error('name')
                        <span class="error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Who can run the command?</label>
                    @foreach ($slackUsers as $user)
                        @if (!$user['is_bot'])
                            <div class="user-row">
                                <div class="pull-right">
                                    <input type="checkbox" name="permission[]" value="{{ $user['id'] }}" {{ isset($user['checked']) && $user['checked'] ? 'checked="checked"' : '' }} />
                                </div>
                                <code>{{ $user['real_name'] }}</code>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                    <button type="submit" class="button"><i class="fa fa-plus"></i> Save Command</button>
                </div>
            </form>
        </div>
    </div>
@endsection
