@extends('layouts.app')

@section('content')
    <h1>App Key: {{ $site->name }}</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-info"></i></div>
        <div class="heading">Site Information</div>
        <div class="content">
            <p>Your App Key: <code>{{ $site->appKey }}</code></p>
            <strong>Do not share your App Key with others.</strong>
            <hr />
            <p>If using Laravel, your app key should be stored in your <code>.env</code> file with the following identifier:</p>
            <code>GILES_APP_KEY={{ $site->appKey }}</code>
        </div>
    </div>
@endsection
