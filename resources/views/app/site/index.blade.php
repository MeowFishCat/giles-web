@extends('layouts.app')

@section('content')
    <style>
        .ib {
            display: inline-block;
            margin-left: 20px;
        }
        ib.i {
            margin-right: 4px;
        }
        .command-row {
            border-bottom: 1px solid #eee;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
    </style>
    <h1>Manage Commands: {{ $site->name }}</h1>
    <p>
        <a href="{{ route('app.site.command.add', [$site->id]) }}" class="button"><i class="fa fa-plus"></i> Add Command</a>
        <a href="{{ route('app.site.edit', [$site->id]) }}" class="ib">Site Settings</a>
        <a href="{{ route('app.site.key', [$site->id]) }}" class="ib">View App Key</a>
    </p>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-terminal"></i></div>
        <div class="heading">Artisan Command Whitelist</div>
        <div class="content">
            @forelse ($site->commands as $command)
                <div class="command-row">
                    <div class="pull-right">
                        <a href="{{ route('app.site.command', ['id' => $command->site->id, 'cid' => $command->id]) }}" class="ib"><i class="fa fa-pencil"></i> Permissions</a>
                        <a href="#" class="ib ml-4"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                    <code>{{ $command->command }}</code>
                </div>
            @empty
                <div class="pull-center space-above-below">No commands to show.<br /><a href="">Create a command</a>.</div>
            @endforelse
        </div>
    </div>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-terminal"></i></div>
        <div class="heading">Bash Commands</div>
        <div class="content">
            <div class="pull-center space-above-below">Bash commands are coming soon!</div>
        </div>
    </div>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-terminal"></i></div>
        <div class="heading">Scheduled Commands</div>
        <div class="content">
            <div class="pull-center space-above-below">Scheduled commands are coming soon!</div>
        </div>
    </div>
@endsection
