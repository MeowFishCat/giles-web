@extends('layouts.app')

@section('content')
    <h1>Dashboard</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-terminal"></i></div>
        <div class="heading">Command History</div>
        <div class="content">
            Card content.
        </div>
    </div>
@endsection
