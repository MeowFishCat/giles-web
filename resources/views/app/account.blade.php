@extends('layouts.app')

@section('content')
    <style>
        .feature .icon {
            float: none!important;
        }
    </style>
    <h1>My Account</h1>
    <div class="card card-with-icon">
        <div class="icon"><i class="fa fa-user"></i></div>
        <div class="heading">Account Information</div>
        <div class="content">
            <div class="feature feature-centered">
                <div class="icon big-icon"><i class="fa fa-info"></i></div>
                <h2>Nothing here yet!</h2>
                <p>This page will eventually display billing information for {{ config('app.name', 'Laravel') }}.</p>
                <p>{{ config('app.name', 'Laravel') }} is <strong>free while in beta</strong>!</p>
            </div>
        </div>
    </div>
@endsection
