@extends('layouts.blank')

@section('content')
    <h1>Login</h1>
    <div class="card card-with-icon">
        <div class="content">
            <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot">
                <img alt="Sign in with Slack" height="40" width="172" class="slack-sign-in"
                     src="https://platform.slack-edge.com/img/sign_in_with_slack.png"
                     srcset="https://platform.slack-edge.com/img/sign_in_with_slack.png 1x,
                        https://platform.slack-edge.com/img/sign_in_with_slack@2x.png 2x"
                />
            </a>
        </div>
    </div>
@endsection
