@extends('layouts.blank')

@section('content')
    <h1>Team Already Registered</h1>
    <div class="card card-with-icon">
        <div class="content">
            <p>It looks as though your team is already registered on {{ config('app.name', 'Laravel') }}, and only the person that set up the account can Login.</p>
            <p>If the person that registered your Slack team no longer works with you, please get in contact with us at <code>contact@giles.app</code>.</p>
            <p>Thank you.</p>
        </div>
    </div>
@endsection
