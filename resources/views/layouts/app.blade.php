<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ isset($title) ? $title . ' - ' : '' }}{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="icon" href="{{ asset('favicon.png') }}" />
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/meow.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;900&display=swap');
        .container {
            max-width: 1000px;
        }
        .main-body {
            margin-top: 40px;
        }
        .logo {
            font-weight: 700;
            font-family: 'Lato', sans-serif;
            color: #3366FF;
            font-size: 24px;
        }
        .card .heading {
            border-bottom: 1px solid #eee;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        .card .content {
            font-size: 14px;
        }
    </style>
</head>
<body class="body-purple body-sidebar">
    <div class="sidebar">
        <div class="logo">
            <i class="fa fa-terminal"></i> {{ config('app.name', 'Laravel') }}
        </div>
        <a href="{{ route('app') }}"><i class="fa fa-tachometer"></i> Dashboard</a>
        <a href="{{ route('app.account') }}"><i class="fa fa-credit-card"></i> My Account</a>
        <a href="{{ route('app.site.add') }}"><i class="fa fa-plus"></i> Add Site</a>
        @foreach (auth()->user()->sites as $site)
            <a href="{{ route('app.site', [$site->id]) }}"><i class="fa fa-hashtag"></i> {{ $site->name }}</a>
        @endforeach
        <a href="/docs"><i class="fa fa-book"></i> Docs</a>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i> Logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>

    <div class="wrapped-content">
        <div class="container">
            <div class="main-body">
            @yield('content')
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>
