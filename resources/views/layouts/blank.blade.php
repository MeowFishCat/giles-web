
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/meow.css') }}" rel="stylesheet">

    <!-- Styles -->
    <style>
        @import url('https://fonts.googleapis.com/css?family=Lato:400,700');
        .logo {
            font-weight: 700;
            font-family: 'Lato', sans-serif;
            color: #3366FF;
            padding: 0 15px 0 15px;
        }
        .justify-content-center {
            display: flex;
            justify-content: center;
        }
    </style>
</head>
<body class="body-purple">
    <div class="container">
        <div class="space-above-below">
            <div class="navigation navigation-with-buttons" id="navigation">
                <div class="navigation-right">
                    <a href="{{ route('index') }}#features">Features</a>
                    <a href="{{ route('index') }}#pricing">Pricing</a>
                    <a href="/docs">Docs</a>
                    <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot" class="button button-emphasis"><i class="fa fa-user"></i> Login</a>
                    <a href="#" class="toggle-icon"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logo"><i class="fa fa-terminal"></i> {{ config('app.name', 'Laravel') }}</div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="space-above-below">
                    @yield('content')
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="pull-right">
                    <a href="{{ route('index') }}#features">Features</a>
                    <a href="{{ route('index') }}#pricing">Pricing</a>
                    <a href="/docs">Docs</a>
                    <a href="https://slack.com/oauth/authorize?client_id=332813230932.1175015023424&scope=commands,users:read,chat:write:bot">Login</a>
                </div>
                <strong>Giles.</strong> &copy; {{ date('Y') }} <a href="https://joshbarker.me" target="_blank">Josh Barker</a>
            </div>
        </footer>
    </div>
</body>
</html>
