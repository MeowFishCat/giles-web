require('./bootstrap');
require('./notiny');

$.notiny.addTheme('scran', {
    'notification_class': 'notiny-theme-scran notiny-default-vars'
});
$.notiny.addTheme('error', {
    'notification_class': 'notiny-theme-error notiny-default-vars'
});