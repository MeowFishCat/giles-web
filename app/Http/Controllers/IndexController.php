<?php
namespace App\Http\Controllers;

class IndexController extends \App\Http\Controllers\Controller
{
    public function index()
    {
        return view('index', [
            'title' => 'Add to Slack'
        ]);
    }
}