<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController
{
    public function index(Request $request)
    {
        $authData = (new \App\Models\Slack\Auth)->post($request->get('code'));

        if (!isset($authData['access_token'])) {
            echo 'Invalid request';
            var_dump($authData);
            return;
        }

        $slackUser = (new \App\Models\Slack\User)->setToken($authData['access_token'])->get($authData['user_id']);

        $user = \App\Models\User::forSlackUser($authData['user_id'])->first();

        //Check if team already exists, if it does- show notification.
        $teamUser = \App\Models\User::forSlackTeam($authData['team_id'])->first();
        if (!$user && $teamUser) {
            return view('auth.team-already-registered');
        }


        if (!$user) {
            $user = \App\Models\User::create([
                'name' => $slackUser['user']['real_name'],
                'email' => 'SLACK_' . md5('GILES_EMAIL_' . (new \DateTime())->format(DATE_ATOM)),
                'password' => Hash::make(md5((new \DateTime())->format(DATE_ATOM))),
                'slack_access_token' => $authData['access_token'],
                'slack_user_id' => $authData['user_id'],
                'slack_team_id' => $authData['team_id'],
                'slack_enterprise_id' => $authData['enterprise_id'],
                'slack_team_name' => $authData['team_name']
            ]);
        }

        $user->slack_access_token = $authData['access_token'];
        $user->save();

        Auth::loginUsingId($user->id, TRUE);

        return redirect()->route('app');
    }
}