<?php
namespace App\Http\Controllers\App;

class AccountController extends \App\Http\Controllers\Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('app.account', [
            'title' => 'My Account'
        ]);
    }
}