<?php
namespace App\Http\Controllers\App\Site;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CommandController extends \App\Http\Controllers\Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function permissions($id, $cid)
    {
        $site = auth()->user()->sites()->find($id);

        if (!$site) {
            return redirect()->route('app');
        }

        $command = $site->commands()->find($cid);

        if (!$command) {
            return redirect()->route('app');
        }

        $users = (new \App\Models\Slack\User)->setToken($command->site->user->slack_access_token)->fetch();

        foreach ($users['members'] as &$slackUser) {
            foreach ($command->permissions as $userWithPermission) {
                if ($slackUser['id'] === $userWithPermission->slack_user_id) {
                    $slackUser['checked'] = true;
                    break;
                }
            }
        }

        return view('app.site.command.permissions', [
            'title' => 'Manage Permissions',
            'command' => $command,
            'users' => $users['members']
        ]);
    }

    public function permissionsPost(Request $request, $id, $cid)
    {
        $site = auth()->user()->sites()->find($id);

        if (!$site) {
            return redirect()->route('app');
        }

        $command = $site->commands()->find($cid);

        if (!$command) {
            return redirect()->route('app');
        }

        $postedPermissions = $request->get('permission');

        if (!$command || $command->site_id != $site->id) {
            return redirect()->route('app');
        }

        \App\Models\Permission::where('command_id', $command->id)->delete();

        if (is_array($postedPermissions)) {
            $this->saveNewPermissions($site, $command, $postedPermissions);
        }

        return $this->permissions($id, $cid);
    }

    public function add($id)
    {
        $user = auth()->user();
        $site = $user->sites()->where('id', $id)->first();

        return view('app.site.command.add', [
            'title' => 'Add Command to ' . $site->name,
            'user' => $user,
            'site' => $site,
            'slackUsers' => (new \App\Models\Slack\User)->setToken($site->user->slack_access_token)->fetch()['members']
        ]);
    }

    public function addPost(Request $request, $id)
    {
        $site = auth()->user()->sites()->find($id);

        if (!$site) {
            return redirect()->route('app');
        }

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->route('app.site.command.add', [$site->id])->withErrors($validator)->withInput();
        }

        $command = \App\Models\Command::create([
            'command' => $request->get('name'),
            'site_id' => $site->id
        ]);

        $postedPermissions = $request->get('permission');
        if (is_array($postedPermissions)) {
            $this->saveNewPermissions($site, $command, $postedPermissions);
        }

        return view('app.site.command.add-post', [
            'title' => 'Add Command to ' . $site->name,
            'site' => $site
        ]);
    }

    private function saveNewPermissions($site, $command, array $postedPermissions): void
    {
        $slackUsers = (new \App\Models\Slack\User)->setToken($site->user->slack_access_token)->fetch();
        $data = [];
        foreach ($slackUsers['members'] as $slackUser) {
            if (in_array($slackUser['id'], $postedPermissions)) {
                $data[] = [
                    'slack_user_id' => $slackUser['id'],
                    'command_id' => $command->id
                ];
            }
        }

        \App\Models\Permission::insert($data);
    }
}
