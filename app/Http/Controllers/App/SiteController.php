<?php
namespace App\Http\Controllers\App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SiteController extends \App\Http\Controllers\Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $user = auth()->user();
        $site = $user->sites()->where('id', $id)->first();

        if (!$site) {
            return redirect()->route('app');
        }

        return view('app.site.index', [
            'title' => 'Manage Commands for ' . $site->name,
            'user' => $user,
            'site' => $site
        ]);
    }

    public function add()
    {
        return view('app.site.add', [
            'title' => 'Add Site'
        ]);
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'regex:/^[a-z-]+$/i'],
            'url' => ['required', 'string', 'regex:/(https:\/\/|http:\/\/)[a-z.\/-]+/i']
        ]);

        if ($validator->fails()) {
            return redirect()->route('app.site.add')->withErrors($validator)->withInput();
        }

        $site = \App\Models\Site::create([
            'user_id' => auth()->user()->id,
            'name' => $request->get('name'),
            'url' => $request->get('url'),
            'appKey' => sha1(time())
        ]);

        //Default commands
        $c1 = \App\Models\Command::create([
            'site_id' => $site->id,
            'command' => 'giles:test'
        ]);
        $c2 = \App\Models\Command::create([
            'site_id' => $site->id,
            'command' => 'down'
        ]);
        $c3 = \App\Models\Command::create([
            'site_id' => $site->id,
            'command' => 'up'
        ]);
        \App\Models\Permission::create(['command_id' => $c1->id, 'slack_user_id' => $site->user->slack_user_id]);
        \App\Models\Permission::create(['command_id' => $c2->id, 'slack_user_id' => $site->user->slack_user_id]);
        \App\Models\Permission::create(['command_id' => $c3->id, 'slack_user_id' => $site->user->slack_user_id]);

        return view('app.site.add-post', [
            'title' => 'Add Site',
            'site' => $site
        ]);
    }

    public function edit($id)
    {
        $site = auth()->user()->sites()->find($id);

        if (!$site) {
            return redirect()->route('app');
        }

        return view('app.site.edit', [
            'title' => 'Edit Site',
            'site' => $site
        ]);
    }

    public function editPost(Request $request, $id)
    {
        $site = auth()->user()->sites()->find($id);

        if (!$site) {
            return redirect()->route('app');
        }

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'regex:/^[a-z-]+$/i'],
            'url' => ['required', 'string', 'regex:/(https:\/\/|http:\/\/)[a-z.\/-]+/i']
        ]);

        if ($validator->fails()) {
            return redirect()->route('app.site.edit', ['id' => $id])->withErrors($validator)->withInput();
        }

        $site->name = $request->get('name');
        $site->url = $request->get('url');
        $site->save();

        return view('app.site.edit-post', [
            'title' => 'Edit Site',
            'site' => $site
        ]);
    }

    public function key($id)
    {
        $site = auth()->user()->sites()->find($id);

        if (!$site) {
            return redirect()->route('app');
        }

        return view('app.site.key', [
            'title' => 'App Key',
            'site' => $site
        ]);
    }
}
