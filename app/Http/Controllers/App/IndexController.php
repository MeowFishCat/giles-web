<?php
namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

class IndexController extends \App\Http\Controllers\Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();

        return view('app.index', [
            'title' => 'Dashboard',
            'user' => $user
        ]);
    }
}
