<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

class ExecuteController
{
    const COMMAND_RESPONSE_NOT_FOUND = 'Sorry, @%s, the given artisan command `%s` was not found.';
    const COMMAND_RESPONSE_SITE_NOT_FOUND = 'Sorry, @%s, the given site `%s` was not found.';
    const COMMAND_RESPONSE_INSUFFICIENT_PERMS = 'Sorry, @%s, you do not have permission to run `%s`.';

    public function execute(Request $request)
    {
        ignore_user_abort(true);

        if (!$request->get('token') || $request->get('token') !== getenv('GILES_API_TOKEN')) {
            return abort(404);
        }

        $teamId = $request->get('teamId');
        $cmdParams = $request->get('cmdParams');
        $originalRequest = json_decode($request->get('originalRequest'), true);

        $responseChannel = $originalRequest['channel_name'];
        $slackUserExecuted = $originalRequest['user_id'];

        $u = \App\Models\User::forSlackTeam($teamId)->first();
        $messagePoster = (new \App\Models\Slack\PostMessage())->setToken($u->slack_access_token);

        try {
            $site = $this->getSite($u, $cmdParams, $originalRequest['user_name']);
        } catch (\Exception $e) {
            $messagePoster->post($responseChannel, $e->getMessage());
            return;
        }

        if ($command = $this->commandExists($site->commands, $this->getCommandString($cmdParams))) {
            if (!$this->userCanExecuteCommand($command, $slackUserExecuted)) {
                $messagePoster->post($responseChannel, sprintf(self::COMMAND_RESPONSE_INSUFFICIENT_PERMS, $originalRequest['user_name'], $cmdParams));
                return;
            }

            $ce = (new \App\Models\Command\CommandExecutor())->call($site, $this->formatCommand($cmdParams));
            $messagePoster->post($responseChannel, $ce['message']);
        } else {
            $messagePoster->post($responseChannel, sprintf(self::COMMAND_RESPONSE_NOT_FOUND, $originalRequest['user_name'], $cmdParams));
        }

        return response('OK');
    }

    private function userCanExecuteCommand($command, $userId)
    {
        foreach ($command->permissions as $userWithAccess) {
            if ($userWithAccess->slack_user_id === $userId) {
                return true;
            }
        }

        return false;
    }

    private function getSiteString($command)
    {
        return explode(' ', trim($command))[0];
    }

    private function getCommandString($command)
    {
        return explode(' ', trim($command))[1];
    }

    private function getSite($user, $command, $slackUserName)
    {
        $commandSite = strtolower($this->getSiteString($command));

        foreach ($user->sites as $uSite) {
            if (strtolower($uSite->name) === $commandSite) {
                return $uSite;
            }
        }

        throw new \Exception(sprintf(self::COMMAND_RESPONSE_SITE_NOT_FOUND, $slackUserName, $this->getSiteString($command)));
    }

    private function commandExists($commands, ?string $commandExecuted)
    {
        foreach($commands as $command) {
            if (strpos($command, $commandExecuted) !== false) {
                $command->called++;
                $command->save();
                return $command;
            }
        }

        return false;
    }

    private function formatCommand($command)
    {
        return substr(strstr($command," "), 1);
    }
}