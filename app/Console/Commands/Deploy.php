<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Deploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploys Giles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->output->write('- Bringing down application.', true);
            $this->call('down');

            $this->output->write('- Pulling changes from git.', true);
            $process = new Process(['git', 'pull']);
            $process->run();
            $this->output->write($process->getOutput());

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $this->output->write('- Running migrations.', true);
            $this->call('migrate', [
                '--env' => 'development',
                '--force' => true
            ]);

            $this->output->write('- Bringing up application.', true);
            $this->call('up');

            $this->output->write(':tada: Giles deployed successfully.', true);
        } catch (\Exception $e) {
            $this->output->write($e->getMessage());
        }
    }
}
