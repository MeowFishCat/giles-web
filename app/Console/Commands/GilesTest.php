<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GilesTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'giles:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Giles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->writeln('*Giles Test Successful!*');
        $this->output->writeln('Giles is now set up and ready to be used on your Slack environment!');
        $this->output->writeln('Visit https://giles.app/docs to learn more about Giles.');
    }
}
