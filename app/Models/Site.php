<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'url',
        'appKey'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function commands()
    {
        return $this->hasMany(Command::class);
    }
}