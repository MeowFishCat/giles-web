<?php
namespace App\Models\Slack;

class Auth extends SlackApiContainer
{
    public function post(string $code)
    {
        $data = $this->request('POST', 'oauth.access', [
            'client_id' => getenv('SLACK_CLIENT_ID'),
            'client_secret' => getenv('SLACK_CLIENT_SECRET'),
            'code' => $code
        ]);

        return json_decode($data, true);
    }
}