<?php
namespace App\Models\Slack;

class User extends SlackApiContainer
{
    public function get(string $userId)
    {
        $data = $this->request('POST', 'users.info', [
            'token' => $this->token,
            'user' => $userId
        ]);

        return json_decode($data, true);
    }

    public function fetch()
    {
        $data = $this->request('POST', 'users.list', [
            'token' => $this->token
        ]);

        return json_decode($data, true);
    }
}