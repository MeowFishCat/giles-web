<?php
namespace App\Models\Slack;

class PostMessage extends SlackApiContainer
{
    public function post(string $channel, string $text)
    {
        $data = $this->request('POST', 'chat.postMessage', [
            'token' => $this->token,
            'channel' => $channel,
            'text' => $text
        ]);

        return json_decode($data, true);
    }
}