<?php
namespace App\Models\Slack;

class SlackApiContainer
{
    const SLACK_API = 'https://slack.com/api/';

    protected $token;

    public function setToken(string $token)
    {
        $this->token = $token;
        return $this;
    }

    protected function request($type, $route, $data)
    {
        $ch = curl_init(self::SLACK_API . $route);
        $data = http_build_query($data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($type));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

}