<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'command_id',
        'slack_user_id'
    ];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}