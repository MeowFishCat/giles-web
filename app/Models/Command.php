<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    protected $fillable = [
        'site_id',
        'command',
        'called'
    ];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}