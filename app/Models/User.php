<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'slack_access_token',
        'slack_user_id',
        'slack_team_id',
        'slack_enterprise_id',
        'slack_team_name'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

    public function scopeForSlackUser($query, $slackUserId)
    {
        return $query->where('slack_user_id', $slackUserId);
    }

    public function scopeForSlackTeam($query, $slackTeamId)
    {
        return $query->where('slack_team_id', $slackTeamId);
    }
}
