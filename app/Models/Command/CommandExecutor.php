<?php
namespace App\Models\Command;

class CommandExecutor
{
    const GILES_API_URL_FORMAT = '%s/giles/call';

    public function call(\App\Models\Site $site, string $command = ''): array
    {
        $ch = curl_init(sprintf(self::GILES_API_URL_FORMAT, $site->url));
        $data = http_build_query([
            'cmdParams' => $command,
            'appKey' => $site->appKey,
        ]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }
}