<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Auth::routes();

/** App */
Route::get('/app', 'App\IndexController@index')->name('app');

// Account
Route::get('/app/account', 'App\AccountController@index')->name('app.account');

// Add site
Route::get('/app/site/add', 'App\SiteController@add')->name('app.site.add');
Route::post('/app/site/add', 'App\SiteController@addPost');

// Edit site
Route::get('/app/site/{id}/edit', 'App\SiteController@edit')->name('app.site.edit');
Route::post('/app/site/{id}/edit', 'App\SiteController@editPost');

// Site app key
Route::get('/app/site/{id}/key', 'App\SiteController@key')->name('app.site.key');

// Add command
Route::get('/app/site/{id}/command/add', 'App\Site\CommandController@add')->name('app.site.command.add');
Route::post('/app/site/{id}/command/add', 'App\Site\CommandController@addPost');

// Manage command permissions
Route::get('/app/site/{id}/command/{cid}', 'App\Site\CommandController@permissions')->name('app.site.command');
Route::post('/app/site/{id}/command/{cid}', 'App\Site\CommandController@permissionsPost');

// View site
Route::get('/app/site/{id}', 'App\SiteController@index')->name('app.site');

Route::get('/auth', 'AuthController@index')->name('auth');